import { Module } from "@nestjs/common";
import { AppController } from "./app.controller";
import { ClientsModule, Transport } from "@nestjs/microservices";
import { AppService } from "./app.service";

@Module({
  imports: [
    ClientsModule.register([
      {
        name: "SERVICE_API_REST",
        transport: Transport.RMQ,
        options: { 
          urls: [`${process.env.CLOUDAMQP_URL || 'amqp://guest:guest@rabbitmq'}`],
          queue: 'api-rest',
          queueOptions: { 
            durable: false
          },
        },
      }
    ])
  ],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule {}

