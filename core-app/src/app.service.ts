import { Injectable, Inject } from "@nestjs/common";
import { ClientProxy } from "@nestjs/microservices";
import { request } from "http";
import { map } from "rxjs/operators";

@Injectable()
export class AppService {
  constructor(
    @Inject("SERVICE_API_REST") private readonly clientApiRest: ClientProxy
  ) {}

  pingServiceA() {
    const startTs = Date.now();
    const pattern = { cmd: "ping" };
    const payload = {};
    return this.clientApiRest
      .send<string>(pattern, payload)
      .pipe(
        map((message: string) => ({ message, duration: Date.now() - startTs }))
      );
  }
  async request2api(cmd, params, payload) {
    const startTs = Date.now();
    const pattern = {
      cmd,
      table: params.table
    } ;
    // const payload = {
    //   params,
    //   body
    // };
    console.log('API CORE', pattern, payload)
    return await this.clientApiRest
      . send<string>(pattern, payload)
      // .pipe(
      //   map((message: string) => ({ message }))
      // );
  }
}
