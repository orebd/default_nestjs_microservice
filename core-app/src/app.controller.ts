import { Req } from "@nestjs/common";
import { Headers } from "@nestjs/common";
import { Post } from "@nestjs/common";
import { Param } from "@nestjs/common";
import { Body } from "@nestjs/common";
import { Controller, Get } from "@nestjs/common";
import { AppService } from "./app.service";

@Controller('api/v1')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get("ping-a")
  pingServiceA() {
    return this.appService.pingServiceA();
  }
  @Post(':table')
  create(@Headers() headers, @Param() params, @Req() req, @Body() body) {
    // console.log(headers, params, req, body)
    return this.appService.request2api( 'create', params, body);
  }
  @Get(':table')
  findAll(@Headers() headers, @Param() params, @Req() req, @Body() body) {
    // console.log(headers, params, req, body)
    return this.appService.request2api( 'findAll', params, body);
  }
  @Get(':table/:id')
  findOne(@Headers() headers, @Param() params, @Req() req, @Body() body) {
    // console.log(headers, params, req, body)
    return this.appService.request2api('findOne', params, params);
  }
}
