import { NestFactory } from "@nestjs/core";
import { Transport } from "@nestjs/microservices";
import { AppModule } from "./app.module";
import { Logger, ValidationPipe } from "@nestjs/common";

const logger = new Logger();

async function bootstrap() {
  const app = await NestFactory.createMicroservice(AppModule, {
    transport: Transport.RMQ,
    options: {
      urls: [`${process.env.CLOUDAMQP_URL || 'amqp://guest:guest@rabbitmq'} `],
      queue: 'api-rest',
      queueOptions: {
        durable: false
      },
    },
  }
  );
  app.useGlobalPipes(new ValidationPipe());
  app.listen(() => logger.log("Microservice A is listening"));
}
bootstrap();
