import { Controller, Get, Post, Body, Put, Param, Delete, PayloadTooLargeException } from '@nestjs/common';
import { HomeworksService } from './homeworks.service';
import { CreateHomeworkDto } from './dto/create-homework.dto';
import { UpdateHomeworkDto } from './dto/update-homework.dto';
import { ApiTags } from '@nestjs/swagger';
import { ValidId } from '../app.controller';
import { Ctx, MessagePattern, Payload, RmqContext } from '@nestjs/microservices';
import { of } from 'rxjs';

@ApiTags('homeworks')
@Controller('homeworks')
export class HomeworksController {
  constructor(private readonly homeworksService: HomeworksService) {}

  @MessagePattern({
    cmd: 'create',
    table: 'homeworks'
  })
  // @Post()
  create(@Body() createHomeworkDto: CreateHomeworkDto) {
    return this.homeworksService.create(createHomeworkDto);
  }

  @MessagePattern({
    cmd: 'findAll',
    table: 'homeworks'
  })
  findAll() {
    return this.homeworksService.findAll()
  }
// ping(_: any) {
//     return of("pong").pipe(delay(1000));
//   }

  @MessagePattern({
    cmd: 'findOne',
    table: 'homeworks'
  })
  // @Get(':id')
  findOne(@Payload() params
  // , @Param() params: ValidId
  ) {
      // params = payload
    return this.homeworksService.findOne(+params.id);
  }

  @MessagePattern({
    cmd: 'update',
    table: 'homeworks'
  })
  // @MessagePattern({method: 'GET'})
  update(@Param() params: ValidId, @Body() updateHomeworkDto: UpdateHomeworkDto) {
    return this.homeworksService.update(+params.id, updateHomeworkDto);
  }

  @MessagePattern({
    cmd: 'remove',
    table: 'homeworks'
  })
  // @MessagePattern({method: 'GET'})
  remove(@Param() params: ValidId) {
    return this.homeworksService.remove(+params.id);
  }
}
