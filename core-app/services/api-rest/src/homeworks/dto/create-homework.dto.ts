import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsEmail, IsNotEmpty } from 'class-validator';

export class CreateHomeworkDto {
    
    @ApiPropertyOptional()
    date: Date;

    @ApiProperty()
    @IsNotEmpty()
    category: string;

    @ApiProperty()
    @IsNotEmpty()
    topic: string;

    @ApiPropertyOptional()
    ownerId: number
}
