import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateHomeworkDto } from './dto/create-homework.dto';
import { UpdateHomeworkDto } from './dto/update-homework.dto';
import { Homework } from './entities/homework.entity';

@Injectable()
export class HomeworksService {
  constructor(
    @InjectRepository(Homework)
    private readonly homeworksRepository: Repository<Homework>,
  ) {}

  create(createHomeworkDto: CreateHomeworkDto) {
    // return 'This action adds a new homework';
    const homework = new Homework();
    homework.date = createHomeworkDto.date;
    homework.category = createHomeworkDto.category;
    homework.topic = createHomeworkDto.topic;
    homework.ownerId = createHomeworkDto.ownerId;

    return this.homeworksRepository.save(homework);
  }

  findAll() {
    // return `This action returns all homeworks`;
    // this.homeworksRepository.find().then(console.log)
    return this.homeworksRepository.find();
  }

  findOne(id: number) {
    // return `This action returns a #${id} homework`;
    return this.homeworksRepository.findOne(id);
  }

  update(id: number, updateHomeworkDto: UpdateHomeworkDto) {
    return `This action updates a #${id} homework`;
  }

  remove(id: number) {
    // return `This action removes a #${id} homework`;
    this.homeworksRepository.delete(id);
  }
}
