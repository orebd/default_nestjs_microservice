import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Homework {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ nullable: true })
    date: Date;

    @Column('text', { nullable: true })
    category: string;

    @Column('text', { nullable: true })
    topic: string;

    @Column('int',  { nullable: true })
    ownerId: number;

}
