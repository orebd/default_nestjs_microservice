import { Controller } from "@nestjs/common";
import { Ctx, MessagePattern, Payload, RmqContext } from "@nestjs/microservices";
import { of } from "rxjs";
import { delay } from "rxjs/operators";

@Controller()
export class AppController {
  @MessagePattern({ cmd: "ping" })
  ping(_: any) {
    return of("pong").pipe(delay(1000));
  }
  // @MessagePattern({method: 'GET'})  
  // getNotifications(@Payload() data: number[], @Ctx() context: RmqContext) {
  //   console.log(`Pattern: ${context.getPattern()}`);
  //   console.log(`Message: ${context.getMessage()}`);
  // }
}

import { IsNumberString, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ValidId {
  @ApiProperty()
  @IsNumberString()
  id: number;
  // @IsString()
  // table: string;
} 