import { NestFactory } from "@nestjs/core";
import { Transport } from "@nestjs/microservices";
import { AppModule } from "./app.module";
import { Logger } from "@nestjs/common";

const logger = new Logger();

async function bootstrap() {
  const app = await NestFactory.createMicroservice(AppModule, {
    transport: Transport.RMQ,
    options: { 
      urls: [`${process.env.CLOUDAMQP_URL}`],
      queue: 'api-rest',
      queueOptions: { 
        durable: false
        },
      },
  });
  app.listen(() => logger.log("Microservice A is listening"));
}
bootstrap();
